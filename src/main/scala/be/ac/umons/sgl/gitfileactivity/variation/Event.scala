package be.ac.umons.sgl.gitfileactivity.variation

import org.joda.time.DateTime

case class Event(file: String, begining: DateTime, end: DateTime)