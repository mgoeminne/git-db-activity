package be.ac.umons.sgl.gitfileactivity

import java.io.{ByteArrayInputStream, File}

import scala.sys.process._

import mgoeminne.scalagit.{Blob, Git}

import scala.sys.process.Process
import scala.xml.{XML, Elem}

/**
 * Catches all the annotations, with the used parameters, if any
 */
object Annotations
{
  val src2srcml = "src2srcml"

  def main(args: Array[String]): Unit =
  {
     val repo = new Git(new File("/Users/Goeminne/git repositories/oscar/.git"))
    process(repo)
  }

  def process(repo: Git): Unit =
  {
    val javaBlobs = repo.allFiles.filter(f => f._1.name endsWith ".java").map(_._2).toSet.par
    val blobAnnotations = javaBlobs.map(b => b.id -> annotations(ast(b))).toMap

    repo.commits.foreach(commit => {
      commit.existingFile().foreach(file => {
        val f = file._1.name
        val annotations = blobAnnotations.getOrElse(file._2.id, Seq())

        annotations.foreach(annotation => {
          println ((
                   repo.directory,
                   commit.id,
                   Main.formatter.print(commit.date),
                   annotation
                    ).productIterator.mkString(","))
        })
      })
    })

  }

  def ast(blob: Blob): Elem =
  {
    val p1 = Process(Seq(src2srcml, "--position", "--language=Java", "--no-namespace-decl", "-"))
    val content = blob  .lines
                        .mkString("\n")
    val is = new ByteArrayInputStream(content.getBytes)
    XML.loadString((p1 #< is).lineStream_!.mkString("").replaceAll("pos:", "pos"))
  }

  def annotations(ast: Elem): Seq[Annotation] =
  {
    Main.logger.trace("annotation extracted")
    (ast \\ "annotation").map( annotation => {
      val column = (annotation \ "@poscolumn").text.toInt
      val line = (annotation \ "@posline").text.toInt

      val name = (annotation \ "name").text
      val parameters = annotation \ "argument_list" \ "argument"

      Annotation(name, column, line, parameters.map(parameter => Parameter.unapply(parameter.text)))
    } )
  }
}

