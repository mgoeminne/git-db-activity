package be.ac.umons.sgl.gitfileactivity.variation

import java.io.File

import be.ac.umons.sgl.gitfileactivity.Main

import scala.io.Source

/**
 * Generates a file per projects showing the number of
 * remained, added, removed and existing files for each commit
 */
object Variation
{
   def main (args: Array[String])
   {
      val directory = new File(args(0))
      val sources = directory.listFiles().filter(_.getAbsolutePath endsWith(".first-last.csv"))
      val projects = sources.map(s => s.getName.split("\\.").head)

      projects.find(p => p == "1ukash_foowebapp").foreach(project => {
         Main.logger.info(project + " : start analysis ")

         val commit_dates = Source.fromFile(new File(directory , project + ".intensity.csv"))
                                  .getLines().drop(1)
                                  .map(l => Main.formatter.parseDateTime(l.split(",")(2)))
                                  .toList.sortBy(_.getMillis)

         val first_last = new File(directory, project + ".first-last.csv")
         val header = Source.fromFile(first_last).getLines().next.split(",")

         val a = Source.fromFile(first_last).getLines().drop(1)
         val entries = a.map(l => Entry(l.split(","), header))

         entries foreach println

         Main.logger.info(project + " : finished")
      })



   }

}
