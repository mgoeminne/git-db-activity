package be.ac.umons.sgl.gitfileactivity

case class Parameter(name: String, value: String)

object Parameter
{
  def unapply(str: String): Parameter =
  {
    val contents = str.split("=")
    if(contents.length > 1)
      Parameter(contents(0).trim, contents.drop(1).mkString("=").trim)
    else
      Parameter("_value", str.trim)
  }
}