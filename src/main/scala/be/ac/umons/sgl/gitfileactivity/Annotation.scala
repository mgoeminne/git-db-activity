package be.ac.umons.sgl.gitfileactivity

case class Annotation(name: String, column: Int, line: Int, parameters: Seq[Parameter])

