package be.ac.umons.sgl.gitfileactivity.variation

import be.ac.umons.sgl.gitfileactivity.Main
import org.joda.time.DateTime
import org.joda.time.format.DateTimeFormat

/**
 * Created by mg on 24/07/15.
 */
case class Entry(file: String, global: (DateTime, DateTime), techno: Map[String, Option[(DateTime, DateTime)]])


object Entry
{
   val complicatedFormatter = DateTimeFormat.forPattern("yyyy-MM-dd'T'HH:mm:ss.SSSZ")

   def apply(line: Array[String], header: Array[String], na: String = "NA"): Entry =
   {
        val OFFSET = 3
       println("hello")
      val technos = header.drop(3).map(_.stripSuffix("-last-date").stripSuffix("-first-date")).distinct
      println("!!! " + technos.mkString(";"))

      line.drop(OFFSET)

      new Entry("file", (complicatedFormatter.parseDateTime(line(1)), complicatedFormatter.parseDateTime(line(2))), Map())
   }
}