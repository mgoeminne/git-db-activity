package be.ac.umons.sgl.gitfileactivity

import java.io.{FileWriter, File, PrintWriter}

import mgoeminne.scalagit
import mgoeminne.scalagit.{Commit, Blob, Git}
import org.joda.time.{LocalDateTime, DateTime}
import org.joda.time.format.DateTimeFormat

import scala.collection.mutable
import scala.collection.parallel.{ParSeq, ParMap, ParSet}
import scala.io.Source
import scala.util.Random

object AlexResults
{
   val IMPORT_REGEX  = """^\s*import\s+(?:static\s+)?([^;]*)""".r

   def main(args: Array[String])
   {
      val doneFile = new File(args(1), "done.log")
      doneFile.createNewFile()

      val uninterestingFile = new File(args(1), "uninteresting.log")
      uninterestingFile.createNewFile()

      val doneOutput = new PrintWriter(new FileWriter(doneFile,true))
      val uninterestingOutput = new PrintWriter(new FileWriter(uninterestingFile,true))

      val skip: Set[String] = Source.fromFile(doneFile).getLines().toSet ++
                              Source.fromFile(uninterestingFile).getLines().toSet



      Main.logger.info("Scanning repositories")
      val directories = new File(args(0)).listFiles().filter(f => (f.getAbsolutePath endsWith ".git") && !(skip contains f.getAbsolutePath))
      Main.logger.info(directories.size + " repositories to analyse")
      val outputDir = new File(args(1))


      Random.shuffle(directories.toSeq).foreach(directory => {
         process(directory, outputDir, uninterestingOutput)
         doneOutput.println(directory.getAbsolutePath)
         doneOutput.flush()
      })

      doneOutput.close()
      uninterestingOutput.close()
   }

   def process(directory: File, outputDir: File, uninterestingOutput: PrintWriter) =
   {
      val repo = new Git(directory)
      Main.logger.info(repo + " : Processing")

      if(!interesting_project(repo))
      {
          Main.logger.info(repo + " : not interesting repository")
          uninterestingOutput.println(directory.getAbsolutePath)
          uninterestingOutput.flush()
      }
      else
      {
         analyse(repo, outputDir)
      }
   }

   private def analyse(repo: Git, outputDir: File): Unit =
   {

      val javaBlobs = repo.allFiles.par.filter(e => e._1.name endsWith ".java").map(_._2)
      val technos = Main.blobPredicates.keySet.toSeq.seq.sorted
      val extensions = Main.filePredicates.keySet.toSeq.seq.sorted

      Main.logger.info(repo + " : generating blob dictionary")
      // Blob -> technos
      val blobDictionary: ParMap[Blob, ParSet[String]] = javaBlobs.map(blob =>
      {
         val impts = imports(blob)
         blob -> Main.blobPredicates.par.filter(entry => entry._2(impts)).keySet
      }).toMap
      Main.logger.info(repo + " : blob dictionary generated")


      /**
       *
       * @param commits a collection of commits
       * @return all the commits being the firsts of each month
       */
      def firstOfMonth(commits: Iterable[Commit]): Iterable[Commit] =
      {
          val format = DateTimeFormat.forPattern("yyyy-MM")
          val candidates = commits.groupBy(c => format.print(c.date)).values
          candidates.map(c => c.reduce(minCommit))
      }

      def configAnalysis(commits: Seq[Commit]): Unit =
      {
         Main.logger.info(repo + " : config files started")

         val configOutput = new PrintWriter(new File(outputDir, repo.toString + ".config.csv"))

         configOutput.println("file,techno,first-commit-date,last-commit-date")
         val fileCommits : mutable.Map[String, (LocalDateTime, LocalDateTime)] = collection.mutable.Map()

         commits.foreach(commit => {
            val date = commit.date
            val files = commit.files.map(f => f.name).filter(f => Main.filePredicates.exists(p => p._2(f)))
            files.foreach(file => {
                  val current = fileCommits.getOrElse(file, (date, date))
                  fileCommits.put(file, (minDate(current._1, date), maxDate(current._2, date)))
            })
         })

         fileCommits.foreach(entry => {
            val name = entry._1
            val extension: String = Main.filePredicates.find(p => p._2(entry._1)).map(_._1).getOrElse("UNKNOWN")

            assert(!(entry._2._1 isAfter entry._2._2), "first commit is after last commit for file " + name)

            configOutput.println(name + "," +
               extension + "," +
               Main.formatter.print(entry._2._1) + "," +
               Main.formatter.print(entry._2._2)
            )
         })

         configOutput.close()
         Main.logger.info(repo + " : config files done")
      }

      def minDate(d1: LocalDateTime, d2: LocalDateTime): LocalDateTime = if(d1 isBefore d2) d1
                                                      else d2


      def maxDate(d1: LocalDateTime, d2: LocalDateTime): LocalDateTime = if(d1 isAfter d2) d1
                                                      else d2

      def minCommit(c1: Commit, c2: Commit): Commit = if(c1.date isBefore c2.date) c1
                                                else c2

      def maxCommit(c1: Commit, c2: Commit): Commit = if(c1.date isAfter c2.date) c1
                                                else c2


      def first_last_analysis(commits: Seq[Commit]): Unit =
      {
         Main.logger.info(repo + " : first-last started")

         val fileFirstLast: mutable.Map[String, mutable.Map[String, (LocalDateTime,LocalDateTime)]] = mutable.Map()
         val fileGlobalFirst = mutable.Map[String, LocalDateTime]()
         val fileGlobalLast = mutable.Map[String, LocalDateTime]()

         /*
          * BIG WARNING
          *
          * Cannot be parallelized, since some dicts are updated commits by commits
          * So a need a lock for almost all the processing of each commit.
          */
         commits.foreach(commit => {
            val date = commit.date
            val files = commit.existingFile()
                              .map(entry => (entry._1.name, entry._2))
                              .filter(entry => entry._1 endsWith ".java")
            //Main.logger.info(repo + ">first-last : processing commit " + Main.formatter.print(commit.date) + " (" + files.size + " files)")


            files.foreach(file => {
               // Main.logger.info(repo + ">first-last : processing file " + file._1)

               val file_name = file._1
               fileGlobalFirst.put(file_name, fileGlobalFirst.get(file_name).map(d => minDate(d,date)).getOrElse(date))
               fileGlobalLast.put(file_name, fileGlobalLast.get(file_name).map(d => maxDate(d,date)).getOrElse(date))

               val detected_technos = blobDictionary.getOrElse(file._2, ParSet[String]())
               detected_technos.foreach(t => {

                  // Main.logger.info(repo + ">first-last : processing techno " + t)


                  val m = fileFirstLast.getOrElseUpdate(file_name, mutable.Map())
                  val current: Option[(LocalDateTime, LocalDateTime)] = m.get(t)
                  val next = current.map(c => (minDate(c._1, date), maxDate(c._2, date))).getOrElse((date,date))
                  m.put(t, next)
               })
            })
         })

         val firstLastOutput = new PrintWriter(new File(outputDir, repo.toString + ".first-last.csv"))

         firstLastOutput.println("file,first-commit-date,last-commit-date," + technos.map(t => t + "-first-date," + t + "-last-date").mkString(","))

         fileFirstLast.foreach(file =>
         {
            val name = file._1
            val data = fileFirstLast.getOrElse(name, mutable.Map())

            val firstGlobal = fileGlobalFirst.get(name)
            val lastGlobal = fileGlobalLast.get(name)

            assert(firstGlobal.isDefined , "global first commit is not defined for file " + name)
            assert(lastGlobal.isDefined , "global last commit is not defined for file " + name)

            assert(!(firstGlobal.get isAfter lastGlobal.get), "global first commit is after global last commit for file " + name)

            firstLastOutput.println(name + "," +
               firstGlobal.map(Main.formatter.print).getOrElse("NA") + "," +
               lastGlobal.map(Main.formatter.print).getOrElse("NA") + "," +

               technos.map(t =>
                  {
                     val techno_val = data.get(t)
                     if(techno_val.isDefined)
                     {
                        assert(!(techno_val.get._1 isAfter techno_val.get._2), "techno " + t + " starts after it finishes for file " + name)
                        assert(!(firstGlobal.get isAfter techno_val.get._1), "techno " + t + " starts before the global first commit for file " + name)
                        assert(!(lastGlobal.get isBefore techno_val.get._2), "techno " + t + " finishes before the global last commit for file " + name)
                     }

                     techno_val.map(v => Main.formatter.print(v._1) + "," + Main.formatter.print(v._2)).getOrElse("NA,NA")

                  }).mkString(",")
               )
         })

         firstLastOutput.close()

         Main.logger.info(repo + " : first-last done")
      }

      def intensity_analysis(commits: Seq[Commit]): Unit =
      {
         Main.logger.info(repo + " : intensity started")

         Main.logger.info(repo + " : computing techno dictionary")
         // Techno -> blobs
         val technoDictionary: ParMap[String, ParSet[Blob]] = blobDictionary.flatMap(entry => entry._2.map(techno => techno -> entry._1))
            .groupBy(entry => entry._1)
            .mapValues(v => v.map(_._2).toSet)
         Main.logger.info(repo + " : techno dictionary computed")

         val intensityOutput = new PrintWriter(new File(outputDir, repo.toString + ".intensity.csv"))
         intensityOutput.println("project,commit,date,nb_java_files," + technos.mkString(",") + "," + extensions.mkString(","))

         commits.foreach(commit =>
         {
            val existingJavaFiles = commit.existingFile.par.filter(_._1.name endsWith ".java")
            val commitJavaBlobs = existingJavaFiles.map(_._2).toSet
            val commitFiles = commit.files

            intensityOutput.println(repo.toString + "," +
               commit.id + "," +
               Main.formatter.print(commit.date) + "," +
               existingJavaFiles.size + "," +
               technos.map(t => (technoDictionary.getOrElse(t, ParSet()) intersect commitJavaBlobs).size).mkString(",") + "," +
               extensions.map(e => commitFiles.filter(a => Main.filePredicates(e)(a.name)).size).mkString(","))
         })

         intensityOutput.close()
         Main.logger.info(repo + " : intensity done")
      }


      val commits = firstOfMonth(repo.commits).toSeq.sorted
      Main.logger.info(repo + " : " + commits.size + " commits selected:\n" + commits.grouped(5).map(g => g.map(c => Main.formatter.print(c.date)).mkString(" ")).mkString("\n"))


      intensity_analysis(commits)

      first_last_analysis(commits)

      configAnalysis(commits)

      Main.logger.info(repo + " : Finished")
   }

   /**
    * @param blob a blob. Must be textual. Is supposed to be contains a java file.
    * @return all the imports declared in the blob.
    */
   def imports(blob: Blob): ParSet[String] =
   {
      try
         {
         blob.lines.par.map(l => IMPORT_REGEX.findFirstMatchIn(l.trim)).flatten.map(_.group(1)).toSet

      }
      catch
       {
            case _: Throwable => ParSet[String]()
       }
   }

   def interesting_project(repo: Git): Boolean =
   {
      val javaBlobs = repo.allFiles.filter(_._1.name endsWith ".java").map(_._2).par

      val conf = repo.files.par.exists(f => Main.filePredicates.exists(p => p._2(f.name)))

      lazy val impt = javaBlobs.exists(blob => {
         val imports = blob.lines.map(l => IMPORT_REGEX.findFirstMatchIn(l.trim)).flatten.map(_.group(1)).toSet
         Main.blobPredicates.par.exists(p => p._2(imports))
      })


      conf || impt
   }
}