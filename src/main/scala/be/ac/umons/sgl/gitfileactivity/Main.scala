package be.ac.umons.sgl.gitfileactivity

import java.io.{FileWriter, PrintWriter, File}
import java.util.regex.Pattern

import com.typesafe.scalalogging.Logger
import mgoeminne.scalagit.{Blob, Git}
import org.joda.time.format.DateTimeFormat
import be.ac.umons.sgl.gitfileactivity.Annotations._
import org.slf4j.LoggerFactory

import scala.collection.parallel.immutable.ParMap
import scala.collection.{GenSet, GenMap}
import scala.io.Source

object Main
{
   val formatter = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss")
   val logger = Logger(LoggerFactory.getLogger("git-file-activity"))

   val blobPredicates: ParMap[String, GenSet[String] => Boolean] = Map(
      "jdbc" -> packagePredicate("java.sql"),
      "jpa" -> packagePredicate("javax.persistence", "java.persistence"),
      "spring-jdbc" -> packagePredicate("org.springframework.jdbc"),
      "sormula" -> packagePredicate("org.sormula"),
      "sql2o" -> packagePredicate("org.sql2o"),
      "jooq" -> packagePredicate("org.jooq"),
      "avaje" -> packagePredicate("com.avaje"),
      "struts1" -> packagePredicate("org.apache.struts."),
      "struts2" -> packagePredicate("org.apache.struts2"),
      "ninja" -> packagePredicate("ninja"),
      "tapestry" -> packagePredicate("tapestry"),
      "wicket" -> packagePredicate("org.apache.wicket"),
      "stripes" -> packagePredicate("net.sourceforge.stripes"),
      "rife" -> packagePredicate("com.uwyn.rife"),
      "vaadin" -> packagePredicate("com.google.gwt"),
      "toplink" -> packagePredicate("oracle.toplink"),
      "apache-ibatis" -> packagePredicate("org.apache.ibatis"),
      "cayenne" -> packagePredicate("org.apache.cayenne"),
      "jorm" -> packagePredicate("org.objectweb.jorm"),
 //     "junit3" ->
 //        { (imports: GenSet[String]) => packagePredicate("junit.framework")(imports) && !packagePredicate("org.junit")(imports) },
 //     "junit4" -> packagePredicate("org.junit"),
      "jaxb" -> packagePredicate("javax.xml.bind"),
      "jdo" -> packagePredicate("javax.jdo"),
      "jodb" -> packagePredicate("com.mobixess.jodb"),
      "object-store" -> packagePredicate("com.odi"),
      "soda" -> packagePredicate("org.odbms"),
      "hibernate" -> packagePredicate("org.hibernate")
   ).par

   val filePredicates: ParMap[String, String => Boolean] = Map(
      "hbm-xml" -> filePredicate(".hbm.xml"),
      "mybatis-xml" -> filePredicate("mybatis.xml", "mybatis-config.xml"),
      "jaxb-xsd" -> filePredicate(".xsd"),
      "jpa-config" -> filePredicate("meta-inf/persistence.xml"),
      "tapestry-config" -> filePredicate("web-inf/web.xml"),
      "struts-config" -> filePredicate("struts-config.xml", "struts.properties", "struts.xml"),
 //     "mvn1" -> filePredicate("project.xml", "maven.xml"),
 //     "mvn2" -> filePredicate("pom.xml"),
 //     "gradle" -> filePredicate("build.gradle"),
 //     "ant" -> filePredicate("build.xml", "build.properties"),
 //     "sbt" -> filePredicate("build.sbt"),
 //     "makefile" -> filePredicate("makefile"),
      "sql" -> filePredicate(".sql"),
      "spring-config" -> filePredicate("spring-config.xml"),
      "spring-beans" -> filePredicate("beans.xml"),
      "jdo-config" -> filePredicate(".jdo")
   ).par

   /**
    * http://stackoverflow.com/a/16258382/1758815
    * @param map1
    * @param map2
    * @tparam K
    * @tparam A
    * @tparam B
    * @return
    */
   private def zipper[K, A, B, C](map1: GenMap[K, A],
                                  map2: GenMap[K, B],
                                  default1: A,
                                  default2: B,
                                  merge: (A, B) => C): GenMap[K, C] =
      {
         for (key <- map1.keys ++ map2.keys)
            yield (key -> merge(map1.getOrElse(key, default1), map2.getOrElse(key, default2)))
      }.toMap


   /**
    *
    * @param pkgs a sequence of packages to find into blobs
    * @return a function that determines if a given blob contains at least 1 line starting with "import [static] P",
    *         where P is one of the considered packages
    */
   private def packagePredicate(pkgs: String*): GenSet[String] => Boolean = imports =>
   {
      imports.exists(i => pkgs.exists(p => i startsWith p))
   }

   private def packagePredicate(pkg: String): GenSet[String] => Boolean = imports =>
   {
      imports.exists(i => i startsWith (pkg))
   }

   /**
    * Builds a predicate for files, based on the end of their names.
    * @param end the string a file name must end with.
    * @return a predicate that checks if a file name ends with the specified string
    */
   private def filePredicate(end: String): String => Boolean = f => f.toLowerCase endsWith end

   private def filePredicate(ends: String*): String => Boolean = f => ends.exists(e => f.toLowerCase endsWith e)

   private def removed[T](previous: GenMap[String, Set[T]], current: GenMap[String, Set[T]]) =
      zipper[String, Set[T], Set[T], Set[T]](previous, current, Set(), Set(), (a, b) => a -- b)

   private def added[T](previous: GenMap[String, Set[T]], current: GenMap[String, Set[T]]) =
      zipper[String, Set[T], Set[T], Set[T]](current, previous, Set(), Set(), (a, b) => a -- b)

   private def maintained[T](previous: GenMap[String, Set[T]], current: GenMap[String, Set[T]]) =
      zipper[String, Set[T], Set[T], Set[T]](current, previous, Set(), Set(), (a, b) => a intersect b)


   def main(args: Array[String])
   {
      //val directories = Seq(new File("/Users/mg/oscar.git"))
      val done = Source.fromFile(new File(args(1), "done.log")).getLines().toSet

      val directories = new File(args(0)).listFiles().filter(f => (f.getAbsolutePath endsWith ".git") && !(done contains f.getAbsolutePath))
      val errorOutput =new PrintWriter(new FileWriter(new File(args(1), "errors.log"),true))
      val doneOutput = new PrintWriter(new FileWriter(new File(args(1), "done.log"),true))

      directories.sortBy(f => f.getName).par.foreach(directory => handle(directory, args, errorOutput, doneOutput))

      errorOutput.close()
      doneOutput.close()
   }

   def handle(directory: File, args: Array[String], errorOutput: PrintWriter, doneOutput: PrintWriter) =
   {
      val repo = new Git(directory)

      val dateOutput = new PrintWriter(new File(args(1), repo + ".date.csv"))
      val fileNameOutput = new PrintWriter(new File(args(1), repo + ".filename.csv"))
      val fileNameShortOutput = new PrintWriter(new File(args(1), repo + ".filename_count.csv"))
      val blobImport = new PrintWriter(new File(args(1), repo + ".blobimport.csv"))
      val commitBlobOutput = new PrintWriter(new File(args(1), repo + ".commitblob.csv"))
      val annotationOutput = new PrintWriter(new File(args(1), repo + ".annotation.csv"))

      try
      {
         process(repo, dateOutput, fileNameOutput, fileNameShortOutput, blobImport, commitBlobOutput, annotationOutput)
         doneOutput.println(directory.getAbsolutePath)
         doneOutput.flush()
      }
      catch
         {
            case e: Error =>
            {
               errorOutput.println(repo + " : " + e)
               logger.error(repo + " : " + e)
            }
         }
      finally
      {
         dateOutput.close()
         fileNameOutput.close()
         fileNameShortOutput.close()
         blobImport.close()
         commitBlobOutput.close()
         annotationOutput.close()
      }
   }

   def process(repo: Git,
               dateOutput: PrintWriter,
               fileNameOutput: PrintWriter,
               fileNameShortOutput: PrintWriter,
               blobImport: PrintWriter,
               commitBlobOutput: PrintWriter,
               annotationOutput: PrintWriter): Unit =
   {
      logger.info(repo + " started")

      val commits = repo.commits

      val javaBlobs = repo.allFiles
         .par
         .filter(e => e._1.name endsWith ".java")
         .map(e => e._2)
         .toSet

      logger.info(repo + " : java blobs extracted")

      def commitBlob(): Unit =
      {
         commitBlobOutput.println("commit,file,blob")

         commits.foreach(commit =>
         {
            commit.existingFile().filter(f => f._1.name endsWith ".java").foreach(f =>
            {
               commitBlobOutput.println(commit.id + "," + f._1.name + "," + f._2.id)
            })
         })

         logger.info(repo + " : commit-blob extracted")
      }

      def blobImports(): Unit =
      {
         logger.info(repo + " : blob imports extraction started")

         val regex = """^\s*import\s+(?:static\s+)?([^;]*)""".r
         val imports: GenMap[Blob, Set[String]] = javaBlobs.par.map(b => b -> b.lines.map(l => regex.findFirstMatchIn(l.trim)).flatten.map(_.group(1)).toSet).toMap
         val technos = blobPredicates.keySet
         blobImport.println("blob," + technos.mkString(","))

         imports.foreach({ case (blob, i) =>
         {
            blobImport.println(blob.id + "," + technos.toSeq.par.map(t => if (blobPredicates(t)(i)) 1 else 0).mkString(","))
         }
         })

         logger.info(repo + " : blob imports extracted")
      }

      def fileNames(): Unit =
      {
         logger.info(repo + " : techno by file name extraction started")

         fileNameOutput.println("commit,techno,file")

         val technos = filePredicates.keySet.toSeq.par
         fileNameShortOutput.println("commit," + technos.mkString(","))

         repo.commits.par.foreach(commit =>
         {
            val files = commit.files.map(_.name).par


            val res = technos.map(t => {
               val filtered = files.filter(filePredicates(t))
               filtered.size
            })

            fileNameShortOutput.println(commit.id + "," + res.mkString(","))

         })
         logger.info(repo + " : techno by file name extracted")
      }


      def dates(): Unit =
      {
         dateOutput.println("repo,commit,date")

         repo.commits.foreach(commit =>
         {
            dateOutput.println(repo + "," + commit.id + "," + formatter.print(commit.date))
         })
         logger.info(repo + " : dates extracted")

      }

      def extractAnnotations(): Unit =
      {
         logger.info(repo + " : annotations extraction started")

         annotationOutput.println("blob,col,line,name,parameters")

         javaBlobs.foreach(blob =>
         {
            val annots = annotations(ast(blob))

            annots.foreach(a =>
            {
               annotationOutput.println(blob.id + "," +
                  a.column + "," +
                  a.line + "," +
                  a.name + "," +
                  a.parameters.map(param => "\"" + param.name + "\"=" + param.value).mkString("{", ",", "}"))
            })
         })

         logger.info(repo + " : annotations extracted")
      }

      dates()
      //commitBlob()
      blobImports()
      fileNames()
      extractAnnotations()

      logger.info(repo + " finished")
   }
}
