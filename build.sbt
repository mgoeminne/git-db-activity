name := "git-file-activity"

version := "1.0"

scalaVersion := "2.11.6"

unmanagedBase <<= baseDirectory { base => base / "libs" }

libraryDependencies += "joda-time" % "joda-time" % "2.8.1"

libraryDependencies += "org.scala-lang.modules" % "scala-xml_2.11" % "1.0.4"

libraryDependencies ++= Seq("com.typesafe.scala-logging" %% "scala-logging" % "3.1.0",
                            "org.slf4j" % "slf4j-api" % "1.7.1",
                            "org.slf4j" % "log4j-over-slf4j" % "1.7.1",  // for any java classes looking for this
                            "ch.qos.logback" % "logback-classic" % "1.0.3")